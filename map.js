var tilesUrl;

if (document.domain == 'localhost') {
	tilesUrl = './prez_2kolo_tiles'
} else {
	tilesUrl = 'https://samizdat.blob.core.windows.net/storage/prez_2kolo_tiles'
};

tilesUrl = './tiles' //odstranit po nahrani na blob

function getPct(part, whole) {
	if (part > 0) {
		return Math.round((part / whole) * 1000) / 10
	} else {
		return 0
	};
};

var colorScale = d3.scale.linear()
	.domain([30.070636, 39.200000, 46.857143, 55.343511, 100])
	.range(['#f2f0f7', '#cbc9e2', '#9e9ac8', '#756bb1', '#54278f'])

var groupLabels = ['do 30 let', 'od 30 do 60 let', 'starších 60 let']

function makeChart(d) {
	var chartData = [getPct(d.rodaci_0_2, d.obyv_0_29), getPct(d.rodaci_30_, d.obyv_30_59), 
						getPct(d.rodaci_60_, d.obyv_60_pl)];

	var svg = d3.select('#sparkline')
		.append('svg')
		.attr('width', 270)
		.attr('height', 65)

	svg.selectAll('rect')
		.data(chartData)
		.enter()
		.append('rect')
		.attr('x', 0)
   		.attr('y', function(d, i) {
   			return (i * 19) + 7;
   		})
   		.attr('width', function(d, i) {
   			return d;
   		})
   		.attr('height', 15)
   		.attr('fill', function(d, i) {
   			return colorScale(d);
   		})

   	svg.selectAll('text')
		.data(chartData)
		.enter()
		.append('text')
		.text(function(d, i) {
        	return d + ' % z osob ' + groupLabels[i];
   		})
   		.attr('x', function(d, i) {
   			return d + 10;
   		})
   		.attr('y', function(d, i) {
   			return (i * 19) + 20;
   		})
};

var map = L.map('map', {
		minZoom: 7,
		maxZoom: 13});

map.scrollWheelZoom.disable();

background = L.tileLayer('https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png', {
	opacity: 1,
    attribution: 'mapová data © přispěvatelé <a target="_blank" href="http://osm.org">OpenStreetMap</a>, obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>'
});

labels = L.tileLayer('https://samizdat.cz/tiles/ton_l2/{z}/{x}/{y}.png', {
	opacity: 1
});

var nehody = L.tileLayer(tilesUrl + '/{z}/{x}/{y}.png', {
	opacity: 0.7,
});

var utfGrid = new L.UtfGrid(tilesUrl + '/{z}/{x}/{y}.grid.json', {
	useJsonP: false
});

utfGrid.on('mouseover', function(e){ info.update(e);}).on('mouseout', function(e){ info.update();})


var info = L.control();
		info.options.position = 'bottomright';
		info.onAdd = function (map) {
		    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
		    this.update();
		    return this._div;
		};

		info.update = function (props) {
			this._div.innerHTML = (props ?
				'<strong>Obec ' + props.data.nazob_1 + ' v okrese ' + props.data.nazok + '</strong><br>'
				+ 'Z ' + props.data.obyv_celke + ' obyvatel je zde <strong>' 
				+ Math.round(props.data.rodaci_pct * 10) / 10 + ' %</strong> rodáků.<br><div id="sparkline"></div>'
				: '<b>Najetím myši vyberte obec.</b>');
			if (props) {
				makeChart(props.data)
			};
		};

var form = document.getElementById("frm-geocode");
var geocoder = null;
var geocodeMarker = null;
form.onsubmit = function(event) {
	event.preventDefault();
	var text = document.getElementById("inp-geocode").value;
	if (text == '') {
		var center = new L.LatLng(49.7417517, 15.3350758);
		map.setView(center, 8);
	} else {
		$.get( "https://api.mapy.cz/geocode?query=" + text, function(data) {
			if (typeof $(data).find('item').attr('x') == 'undefined') {
				alert("Bohužel, danou adresu nebylo možné najít");
				return;
			}
			var latlng = new L.LatLng($(data).find('item').attr('y'), $(data).find('item').attr('x'));
			map.setView(latlng, 15);
		}, 'xml');
	}	
};

map.setView([49.7417517, 15.3350758], 8)
			.addLayer(background)
			.addLayer(nehody)
			.addLayer(labels)
			.addLayer(utfGrid)
			.addControl(info)
			